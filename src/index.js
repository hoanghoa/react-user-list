import React from "react";
import ReactDOM from "react-dom";
import ListUser from "./components/ListUser";
import "bulma/css/bulma.css";
import "./index.css";
ReactDOM.render(
  <React.StrictMode>
    <ListUser />
  </React.StrictMode>,
  document.getElementById("root")
);
