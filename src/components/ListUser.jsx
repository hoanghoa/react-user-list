import React, { useState } from "react";
import InputForm from "./InputForm";

const ListUser = () => {
  const [users, setUsers] = useState([]);
  const [isOpen, setIsOpen] = useState(false);
  const [userEdit, setUserEdit] = useState(null);

  const handleDeleteItem = (i) => {
    users.splice(i, 1);
    setUsers([...users]);
  };

  const handleEditItem = (item, index) => {
    setIsOpen(true);
    setUserEdit({ item, index });
  };

  const submitHandler = (data) => {
    const { formData, action, index } = data;
    if (action === "Create") {
      setUsers([...users, formData]);
    }

    if (action === "Edit") {
      users[index] = formData;
      setUsers([...users]);
    }
  };

  const handleCloseForm = () => {
    setIsOpen(false);
    setUserEdit(null);
  };

  return (
    <div className="container">
      <button className="button is-primary" onClick={() => setIsOpen(true)}>
        Create
      </button>
      <table className="table is-bordered is-striped is-hoverable is-fullwidth">
        <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Job</th>
            <th>Gender</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {users.map((item, index) => (
            <tr key={index}>
              <td>{item.name}</td>
              <td>{item.email}</td>
              <td>{item.job}</td>
              <td>{item.gender}</td>
              <td>
                <div className="buttons">
                  <button
                    className="button is-success is-small"
                    onClick={() => handleEditItem(item, index)}
                  >
                    Edit
                  </button>
                  <button
                    className="button is-danger is-small"
                    onClick={() => handleDeleteItem(index)}
                  >
                    Delete
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <div className={`modal ${isOpen ? "is-active" : ""}`}>
        <div
          className="modal-background"
          style={{ backgroundColor: "rgba(10, 10, 10, 0.6)" }}
          onClick={handleCloseForm}
        ></div>
        <div className="modal-content">
          <div className="box">
            <InputForm
              onSubmitForm={submitHandler}
              onClose={handleCloseForm}
              userEdit={userEdit}
            />
          </div>
        </div>
      </div>
      <div className="columns is-mobile">
        <div className="column"></div>
        <div className="column"></div>
      </div>
    </div>
  );
};

export default ListUser;
