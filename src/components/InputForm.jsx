import React, { useEffect, useState } from "react";

const InputForm = (props) => {
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    job: "",
    gender: "",
  });

  const [error, setError] = useState(null);

  const [action, setAction] = useState("Create");
  const [index, setIndex] = useState(0);
  useEffect(() => {
    if (props.userEdit) {
      const { item, index } = props.userEdit;
      setFormData(item);
      setAction("Edit");
      setIndex(index);
    }
    return () => {
      setFormData({});
      setAction("Create");
    };
  }, [props.userEdit]);

  const submitHandler = (e) => {
    e.preventDefault();
    let formIsValid = false;
    const { name, email, job, gender } = formData;

    if (!name || name.length === 0) {
      setError({ ...error, name: "Name is required" });
    } else {
      formIsValid = true;
    }
    if (!email || email.length === 0) {
      setError({ ...error, email: "Email is required" });
    } else {
      formIsValid = true;
    }

    if (!gender) {
      setError({ ...error, gender: "Gender is required" });
    } else {
      formIsValid = true;
    }

    if (!job || job.length === 0) {
      setError({ ...error, job: "Job is required" });
    } else {
      formIsValid = true;
    }

    if (formIsValid) {
      props.onSubmitForm({ formData, action: action, index: index });
      setError(null);
    }
  };

  const inputChangeHandler = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleClose = () => {
    props.onClose();
  };
  return (
    <form onSubmit={submitHandler}>
      <div className="field">
        <label className="label">Name</label>
        <div className="control">
          <input
            className="input"
            type="text"
            placeholder="Name"
            name="name"
            value={formData.name}
            onChange={inputChangeHandler}
          />
        </div>
        {error?.name && <p className="has-text-danger">{error.name}</p>}
      </div>
      <div className="field">
        <label className="label">Email</label>
        <div className="control">
          <input
            className="input"
            type="email"
            placeholder="Email"
            name="email"
            value={formData.email}
            onChange={inputChangeHandler}
          />
        </div>
        {error?.email && <p className="has-text-danger">{error.email}</p>}
      </div>
      <div className="field">
        <label className="label">Job</label>
        <div className="control">
          <div className="select">
            <select
              name="job"
              value={formData.job}
              onChange={inputChangeHandler}
            >
              <option>-- Select job --</option>
              <option value="Student">Student</option>
              <option value="Deverloper">Deverloper</option>
            </select>
          </div>
        </div>
        {error?.job && <p className="has-text-danger">{error.job}</p>}
      </div>
      <div className="field">
        <div className="control">
          <label className="radio">
            <input
              type="radio"
              name="gender"
              value="Male"
              checked={formData.gender === "Male"}
              onChange={inputChangeHandler}
            />{" "}
            Male
          </label>
          <label className="radio">
            <input
              type="radio"
              name="gender"
              value="Female"
              checked={formData.gender === "Female"}
              onChange={inputChangeHandler}
            />{" "}
            Female
          </label>
        </div>
        {error?.gender && <p className="has-text-danger">{error.gender}</p>}
      </div>
      <div className="field is-grouped">
        <div className="control">
          <button type="submit" className="button is-link">
            {action}
          </button>
        </div>
        <div className="control">
          <button className="button is-link is-light" onClick={handleClose}>
            Cancel
          </button>
        </div>
      </div>
    </form>
  );
};

export default InputForm;
